﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReBusTestCommon
{

    public enum UnderlayingReceptor
    {
        Rabbit = 0,
        SqlServer = 1,
        PgSql = 2,
        InMemory = 3
    }
}
