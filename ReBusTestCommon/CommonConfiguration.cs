﻿using Rebus.Activation;
using Rebus.Config;
using Rebus.Logging;
using Rebus.NLog;
using Rebus.Transport.InMem;

namespace ReBusTestCommon
{
    public static class CommonConfiguration
    {
        public static LogLevel GeneralLogLevel = LogLevel.Warn;

        public static UnderlayingReceptor UnderlayerTransport = UnderlayingReceptor.SqlServer;
        private static InMemNetwork _inMemNetwork;

        public static RebusConfigurer ConfigureCommon(BuiltinHandlerActivator adapter)
        {
            var result = Configure.With(adapter)
            .Logging(l => l.NLog())
            //.Logging(l => l.ColoredConsole(GeneralLogLevel))
            ;
            return result;
        }

        public static InMemNetwork InMemNetwork()
        {
            if (_inMemNetwork == null)
                _inMemNetwork = new InMemNetwork();

            return _inMemNetwork;
        }
    }
}
