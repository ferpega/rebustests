﻿using Rebus.Activation;
using System;
using Rebus.Config;
using System.Threading.Tasks;
using Rebus.SqlServer.Transport;
using Rebus.PostgreSql.Transport;
using System.Collections.Generic;
using ReBusTestCommon;
using NLog;
using Rebus.Transport.InMem;
using NLog.Config;
using NLog.Targets;

namespace ReBusTestInput
{
    public class InputProgram
    {
        private static Logger logger = LogManager.GetLogger("NormalLog");

        private static Rebus.Logging.LogLevel GeneralLogLevel = Rebus.Logging.LogLevel.Warn;

        static void Main()
        {
            logger.Error("This is an INPUT log error line");
            //// configure NLog
            //var configuration = new LoggingConfiguration
            //{
            //    LoggingRules = { new LoggingRule("*", NLog.LogLevel.Debug, new ConsoleTarget("console")) }
            //};

            // LogManager.Configuration = configuration;
            using (var adapter = new BuiltinHandlerActivator())
            {
                ConfigureAdapter(adapter);
                
                adapter.Handle<string>(async message =>
                {
                    if (message.Equals("exit"))
                    {                        
                        Environment.Exit(0);
                        //adapter.Bus.Unsubscribe<string>().Wait();
                    }

                    Console.WriteLine(message);
                    await Task.Delay(200);
                });
                
                //adapter.Bus.Subscribe<string>().Wait();

                Console.WriteLine("[INPUT] - Consumer listening - press ENTER to quit");
                Console.ReadLine();
            }
        }

        private static void ConfigureAdapter(BuiltinHandlerActivator adapter)
        {
            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.Rabbit))
                ConfigureWithRabbit(adapter);

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.SqlServer))
                ConfigureWithSqlServer(adapter);

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.PgSql))
                ConfigureWithPgSql(adapter);

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.InMemory))
                ConfigureWithInMemory(adapter);
        }

        private static void ConfigureWithRabbit(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UseRabbitMq("amqp://localhost:32769", "defaultQueue")
                .AddClientProperties(new Dictionary<string, string>
                        {
                            {"durable", "true"},
                            {"exclusive", "false"},
                            {"autoDelete", "false"},
                        }))
                .Start();
        }

        private static void ConfigureWithSqlServer(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UseSqlServer(@"Data Source=(localdb)\mssqllocaldb;Initial Catalog=ReBusDb;Integrated Security=True", "defaultTable", "consumer"))
                .Start();
        }

        private static void ConfigureWithPgSql(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UsePostgreSql("server=localhost;port=15432;database=rebus2_test;user id=postgres; password=Patata.123;maximum pool size=30", "defaultTable", "consumer"))
                .Start();
        }

        private static void ConfigureWithInMemory(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UseInMemoryTransport(CommonConfiguration.InMemNetwork(), "InMemory2"))
                //.Routing(r => r.TypeBased().Map<string>("consumer"))
                .Start();
        }

        //private static Task HandleX(IBus arg1, IMessageContext arg2, string arg3)
        //{
        //    Console.WriteLine(arg3);
        //    return Task.Delay(100);
        //}
    }
    
}
