﻿using Rebus.Activation;
using Rebus.Config;
using Rebus.Logging;
using System;
using Rebus.Bus;
using Rebus.SqlServer.Transport;
using Rebus.Routing.TypeBased;
using Rebus.PostgreSql.Transport;
using System.Threading;
using System.Collections.Generic;
using ReBusTestCommon;
using NLog;
using Rebus.Transport.InMem;
using NLog.Config;
using NLog.Targets;

namespace ReBusTestOutput
{
    public class OutputProgram
    {
        private static Logger logger = LogManager.GetLogger("NormalLog");

        private static Rebus.Logging.LogLevel GeneralLogLevel = Rebus.Logging.LogLevel.Warn;

        static void Main()
        {
            logger.Error("This is an OUTPUT log error line");
            using (var adapter = new BuiltinHandlerActivator())
            {
                ConfigureAdapter(adapter);

                var keepRunning = true;

                while (keepRunning)
                {
                    Console.WriteLine("[OUTPUT] - Press 'any' key to publish a job.  'x' to exit.");
                    var key = char.ToLower(Console.ReadKey(true).KeyChar);

                    if (key.Equals('x'))
                    {
                        //Publish(adapter.Bus, "exit");
                        Publish(adapter.Bus, "exit", 10);
                        Thread.Sleep(5000);
                        keepRunning = false;
                    }
                    else
                    {
                        //Publish(adapter.Bus, $"[OUTPUT] - Publicando........... {DateTime.Now.ToString()}");
                        Publish(adapter.Bus, $"[OUTPUT] - Publicando........... {DateTime.Now.ToString()}", 2);
                    }
                }
            }
        }

        private static void ConfigureAdapter(BuiltinHandlerActivator adapter)
        {
            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.Rabbit))
                ConfigureWithRabbit(adapter);

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.SqlServer))
                ConfigureWithSqlServer(adapter);

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.PgSql))
                ConfigureWithPgSql(adapter);

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.InMemory))
                ConfigureWithInMemory(adapter);
        }

        private static void ConfigureWithRabbit(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UseRabbitMq("amqp://localhost:32769", "defaultQueue")
                .AddClientProperties(new Dictionary<string, string>
                        {
                            {"durable", "true"},
                            {"exclusive", "false"},
                            {"autoDelete", "false"},
                        }))
                .Start();
        }

        private static void ConfigureWithSqlServer(BuiltinHandlerActivator adapter)
        {
            var localDb = @"Data Source=(localdb)\mssqllocaldb;Initial Catalog=ReBusDb;Integrated Security=True";

            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UseSqlServerAsOneWayClient(localDb, "defaultTable"))
                //.Timeouts(t => t.StoreInSqlServer(localDb, "RebusTimeouts"))
                .Routing(r => r.TypeBased().Map<string>("consumer"))
                .Start();
        }

        private static void ConfigureWithPgSql(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UsePostgreSql("server=localhost;port=15432;database=rebus2_test;user id=postgres; password=Patata.123;maximum pool size=30", "defaultTable", "consumer"))
                //.Routing(r => r.TypeBased().Map<string>("consumer"))
                .Start();
        }

        private static void ConfigureWithInMemory(BuiltinHandlerActivator adapter)
        {
            var configurer = CommonConfiguration.ConfigureCommon(adapter);
            configurer
                .Transport(t => t.UseInMemoryTransport(CommonConfiguration.InMemNetwork(), "InMemory1"))
                //.Routing(r => r.TypeBased().Map<string>("consumer"))
                .Start();
        }

        private static void Publish(IBus bus, string message)
        {
            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.Rabbit))
                bus.Publish(message).Wait();

            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.SqlServer) ||
                CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.PgSql))
                bus.Send(message).Wait();
        }

        private static void Publish(IBus bus, string message, int timeoutInSeconds)
        {
            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.Rabbit))
            {
                bus.Defer(TimeSpan.FromSeconds(timeoutInSeconds), message).Wait();
                return;
            }


            if (CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.SqlServer) ||
                CommonConfiguration.UnderlayerTransport.Equals(UnderlayingReceptor.PgSql))
            {
                bus.Defer(
                    TimeSpan.FromSeconds(timeoutInSeconds), 
                    message,
                    new Dictionary<string, string>
                    {
                        { "rbs2-defer-recipient" , "consumer"}
                    }).Wait();
            }
        }
    }
    
}
